#!/usr/bin/env python

# Copyright (C) 2013 Dipongkar Talukder
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

""" This file is part of LIGO Channel Activity Monitor (LigoCAM)."""

from __future__ import division
import numpy as np
import sys
import os
import shutil
import fileinput

from gwpy.time import from_gps
from gwpy.timeseries import TimeSeries
from gwpy.astro import inspiral_range

__author__ = 'Dipongkar Talukder <dipongkar.talukder@ligo.org>'

ALLOW_TAPE = 0
BLRMS_IDX = -4
STATUS_IDX = -2

# =============================================================================


def get_data(channel, time, duration, allow_tape=ALLOW_TAPE):
    """
    Fetch GWPy time series and compute single-avg power spectral density.
    """

    ts = TimeSeries.fetch(
        channel, time, time + duration, allow_tape=allow_tape)
    psd = ts.psd()
    return ts.value, psd.value, psd.frequencies.value


def get_binned(x, b):
    """
    Linearly bin a power spectrum.

    Parameters
    ----------
    x : array
        Power spectral density to be binned.
    b : int
        Bin width in number of bins.

    Returns
    -------
    x_out : array
        Binned PSD values.
    """

    N = len(x) // b
    b_edge = len(x) - (b * N)
    x_out = x[:b * N].reshape((-1, b)).mean(axis=1)
    if b_edge > 0:
        x_edge = x[b*N:].reshape((-1, b_edge)).mean(axis=1)
        x_out = np.concatenate([x_out, x_edge], axis=0)
    return x_out


def filter_magnetometers(results_file):
    """
    Make sure magnetometers are  only flagged as disconnected if all
    three axes are. Note that they can still be flagged as DAQ failure
    since that is independent of the magnetometer.
    """

    with open(results_file, 'r') as f:
        lines = f.readlines()
    channels = [x.split(',')[0] for x in lines]
    locations = [
        channel.rstrip('_DQ').rstrip('_X').rstrip('_Y').rstrip('_Z')
        for channel in channels if '_MAG' in channel
    ]
    if len(locations) == 0:
        return
    locations = sorted(set(locations))
    ok_channels = []
    for loc in locations:
        disconnected = 0
        for line in lines:
            if loc in line and line.split(',')[-2] == 'disconn':
                disconnected += 1
        if disconnected < 3:
            for i, line in enumerate(lines):
                if loc in line:
                    split = line.split(',')
                    split[-2] = 'ok'
                    split[-1] = '0\n'
                    lines[i] = ','.join(split)
                    ok_channels.append(split[0])
    with open(results_file, 'w') as f:
        f.writelines(sorted(lines))
    return ok_channels


def filter_results(results_file, blrms_idx=BLRMS_IDX, status_idx=STATUS_IDX):
    """
    Sort a results file by alert status and BLRMS excess.
    """

    with open(results_file, 'r') as f:
        lines = f.readlines()
    new_1, new_2, new_3, new_4 = [[], [], [], []]
    for line in lines:
        values = line.split(',')
        blrms = values[blrms_idx]
        status = values[status_idx]
        if status != 'ok':
            if blrms == 'excess':
                new_1.append(line)
            else:
                new_2.append(line)
        elif blrms == 'excess':
            new_3.append(line)
        else:
            new_4.append(line)
    new_lines = (sorted(new_1) + sorted(new_2) +
                 sorted(new_3) + sorted(new_4))
    with open(results_file, 'w') as f:
        f.writelines(new_lines)
    return


def edit_calendar(calendar_file, results_url, current_gps):
    """
    Add a results url link to calendar file.
    """

    ymdh = from_gps(current_gps).strftime('%Y%m%d%H')
    hour = ymdh[-2:]
    calendar_temp = '%s_%s.html' % (calendar_file.rstrip('.html'), current_gps)

    # String to be replaced
    stringold = ('<!-- {} --> <li class="sgrayl l1"><p>{}:00</p></li>'
                 .format(ymdh, hour))
    stringnew = ('<li class="greenish l1"><p><a href="{}">{}:00</a></p></li>'
                 .format(results_url, hour))
    shutil.copy2(calendar_file, calendar_temp)
    for j, line in enumerate(fileinput.input(calendar_temp, inplace=1)):
        sys.stdout.write(line.replace(stringold, stringnew))
    shutil.copy2(calendar_temp, calendar_file)
    os.remove(calendar_temp)
    return


def get_inspiral_range(
        ifo, time, duration, fftlength=4, allow_tape=ALLOW_TAPE):
    """
    Get inspiral range of interferometer in order to determine whether to
    send BLRMS change reports.
    """

    ifo_dict = {'LHO': 'H1', 'LLO': 'L1'}
    try:
        ifo = ifo_dict[ifo]
    except KeyError:
        pass
    ts = TimeSeries.fetch(
        '{}:GDS-CALIB_STRAIN'.format(ifo), time,
        time + duration, allow_tape=allow_tape)
    # Get inspiral range from PSD
    psd = ts.psd(fftlength=fftlength)
    if psd.value.min() > 1e-99:
        return inspiral_range(psd, fmin=10).value
    else:
        return 0
