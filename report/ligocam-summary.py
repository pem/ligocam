#!/usr/bin/env python
# Author: Benjamin Mannix (2023)

"""
This script is desinged to take information from the results of LigoCAM
and create a plot of the channels that have been flagged with alert.
This bar graph shows all the channels with problems, what type of problem
they have, and how long they've had this problem. 

Inputs:
Config file (to get run directory of LigoCAM)
Time (default = 'now')
Results file (default = 'out_dir/results/results_current.txt')

Outputs:
Saves plot as png in 'run_dir/summary'
"""

import pandas as pd
import matplotlib.pyplot as plt
import datetime
import os

from argparse import ArgumentParser

try:
    from configparser import ConfigParser
except ImportError:  # python 2.x
    from ConfigParser import ConfigParser

from gwpy.time import tconvert, from_gps, to_gps

__author__ = 'Benjamin Mannix <benjaminrobert.mannix@ligo.org>'

# =============================================================================


# Argument parsing from command line
argparser = ArgumentParser()
argparser.add_argument('config_file', help="LigoCAM configuration file.")
argparser.add_argument('-t', '--time',
                       help="Analysis time (GPS or UTC). Defaults to " +
                       "current time.")
argparser.add_argument('-r', '--results',
                       help="Results text file that LigoCAM outputs. " +
                        "Defaults to run_dir/results/results_current.txt")
args = argparser.parse_args()
config_file = args.config_file

# Config parsing from config file
config = ConfigParser()
config.read(config_file)
out_dir = config.get('Paths', 'out_dir')

# Current time
if args.time is None:
    current_time = tconvert()
else:
    current_time = to_gps(args.time)
current_time_utc = from_gps(current_time)


# typically will be run between 23:00 and 00:00 so plot exists at start of the corresponding UTC day 
# However we want to display the right day 
if current_time_utc.hour == 23:
    current_time_utc = current_time_utc + datetime.timedelta(days=1)

year = str(current_time_utc.year)
month = f"{current_time_utc:%m}"
day = f"{current_time_utc:%d}"

# create year_month directory where plots will populate
summary_dir = out_dir + '/summary/' 
month_dir = summary_dir +  year + '_' + month + '/'

if not os.path.isdir(month_dir):
    os.makedirs(month_dir)

# The text file has no headings, so I'll add them
column_labels = ['Channel','0.03-0.1','0.1-0.3','0.3-1','1-3','3-10','10-30','30-100',
                 '100-300', '300-1000', '1000-3000','3000-10000','BLRMS Change?',
                 'BLRMS Time','Problem?', 'Problem Time']
# the problem column will have either disconn, hardware, or daqfail for each of the problem conditions (or 'ok' if there are no issues)

# parse results .txt file from arguments and read it into dataframe
results = args.results
if args.results == None:
    results = out_dir +  'results/results_current.txt'

# read in results .txt file
results_df = pd.read_csv(results, names = column_labels)

#take only the channels that have problems 
results_problems = results_df.loc[results_df['Problem?'] != 'ok']

# Scrub the channel list of channels LigoCAM doesn't accurately report the status of 
# For some channels, we may add exceptions in the future so LigoCAM accurately reports their status
# For others, we will add a third neutral category to the status column 
results_problems = results_problems[ ~results_problems.Channel.str.contains("_ADC_|_RADIO_|_TEMPERATURE_|_VMON_|_AMON_")]
results_problems = results_problems.reset_index()

# color coding the bars for each failure case
colors = []
for i in range(len(results_problems)):
    if results_problems['Problem?'][i] == 'disconn':
        colors = colors + ['red']
    elif results_problems['Problem?'][i] == 'daqfail':
        colors = colors + ['orange']
    else:
        colors = colors + ['blue']

# this bit makes sure the legend doesn't add a label for every channel/doesn't repeat the same failure conditions 
colors_legend = {'Disconnected':'red', 'DAQ Failure':'orange','Saturating':'blue'}         
labels = list(colors_legend.keys())
handles = [plt.Rectangle((0,0),1,1, color=colors_legend[label]) for label in labels]

#the figure height should be dynamic in case many channels have issues (after 30 channels is when the plot starts getting clustered)
# note: starts getting clustered again around 80 channels but if that many channels have problems this won't be the first concern
fig_x = 8  # plt default x
fig_y = 6  # plt default y
if len(results_problems) > 30:
    fig_y = 6 + ( len(results_problems))/10
fig = plt.figure(figsize =(fig_x,fig_y))

# set up axes and tick labels
ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
ax.set_xticks([20,40,60,80,100])
ax.set_xticklabels(['20','40','60','80','>100'])

# plot the the problem time of every channel as a bar 
ax.barh(results_problems['Channel'],results_problems['Problem Time'], color=colors, label = labels, edgecolor= 'k', zorder = 3)
ax.set_xlim(0,100)
ax.set_xlabel("Hours with problem", fontsize = 12)

ax.legend(handles, labels)


ax.set_title("Problems with PEM channels reported by LigoCAM (" + year + '-' + month + '-' + day + ')', fontsize =14)
ax.grid(zorder=0)

plot_name = 'ligocam_summary_' + year + '_' + month + '_' + day

plt.savefig(month_dir + plot_name, bbox_inches="tight")