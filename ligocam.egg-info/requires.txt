six>=1.16.0
numpy>=1.21.4
scipy>=1.7.3
matplotlib>=3.5.1
astropy>=5.0
gwpy>=2.1.2
lscsoft-glue>=2.0.0
