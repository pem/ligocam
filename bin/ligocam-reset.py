#!/usr/bin/env python
# Copyright (C) 2013 Dipongkar Talukder
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import os
import glob
from argparse import ArgumentParser
from configparser import ConfigParser
from ligocam import alert as lcalert
from numpy import loadtxt

__author__ = 'Philippe Nguyen <philippe.nguyen@ligo.org>'

# Parse arguments
argparser = ArgumentParser()
argparser.add_argument('config_file', help="Configuration file for run")
argparser.add_argument('channel', help="Channel or list of channels to reset")
args = argparser.parse_args()
config_file = args.config_file
channel = args.channel

# Parse config
config = ConfigParser()
config.read(args.config_file)
run_dir = config.get('Paths', 'run_dir')
hist_dir = os.path.join(run_dir, 'history')

# Check first if it's a list of channels
if channel.endswith('txt') or channel.endswith('csv') :
    channels =  loadtxt(channel, dtype='str')
    for i in range(len(channels)):
        # Remove channel from history file
        hist_file = os.path.join(hist_dir, 'history.txt')
        lcalert.reset_history(hist_file, channels[i])

        # Remove channel reference PSD
        glob_pathname = os.path.join(
            hist_dir, channels[i].rstrip('_DQ').replace(':', '_') + '*.txt')
        channel_files = glob.glob(glob_pathname)
        if len(channel_files) > 0:
            for channel_file in channel_files:
                os.remove(os.path.join(hist_dir, channel_file))
        else:
            print("Channel {} has no reference PSD to delete.".format(channels[i]))

# Reset for only one channel
else:
    # Remove channel from history file
    hist_file = os.path.join(hist_dir, 'history.txt')
    lcalert.reset_history(hist_file, channel)

    # Remove channel reference PSD
    glob_pathname = os.path.join(
        hist_dir, channel.rstrip('_DQ').replace(':', '_') + '*.txt')
    channel_files = glob.glob(glob_pathname)
    if len(channel_files) > 0:
        for channel_file in channel_files:
            os.remove(os.path.join(hist_dir, channel_file))
    else:
        print("Channel {} has no reference PSD to delete.".format(channel))
