#!/usr/bin/env python
# Copyright (C) 2013 Dipongkar Talukder
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""
LIGO Channel Activity Monitor (LigoCAM) analyzes power spectra of auxiliary
channels and flags those that show signs of DAQ failure, disconnection, or
significant band-limited RMS changes. This script performs post-processing of
ligocam results, moving the data to public HTML pages and creating calendars
and tables for viewing.
"""

import os
import shutil
from argparse import ArgumentParser

try:
    from configparser import ConfigParser
except ImportError:  # python 2.x
    from ConfigParser import ConfigParser

from gwpy.time import from_gps

import plugins.utils as lcutils
import plugins.html as lchtml
import plugins.alert as lcalert

__author__ = 'Philippe Nguyen <philippe.nguyen@ligo.org>'

# =============================================================================

# Argument parsing
argparser = ArgumentParser()
argparser.add_argument('-c', '--config-file',
                       help="LigoCAM configuration file.")
argparser.add_argument('-t', '--current-time', type=int,
                       help="Current GPS time.")
argparser.add_argument('channel_list',
                       help="Channel list.")
args = argparser.parse_args()
config_file = args.config_file
current_time = args.current_time
channel_list = args.channel_list
utc_fmt = '%h %d %Y %H:%M:%S UTC'
current_time_utc = from_gps(current_time).strftime(utc_fmt)
year_month_str = from_gps(current_time).strftime('%Y_%m')

# Config parsing
config = ConfigParser()
config.read(config_file)
ifo = config.get('Run', 'ifo')
subsystem = config.get('Run', 'subsystem')
duration = int(config.get('Run', 'duration'))
email_to_1 = config.get('Email', 'email_1')
email_to_2 = config.get('Email', 'email_2')
email_from = config.get('Email', 'from')
email_replyto = config.get('Email', 'reply_to')
email_disconn = config.get('Email', 'disconn_hour')
email_daqfail = config.get('Email', 'daqfail_hour')
email_hardware = config.get('Email', 'saturate_hour')
email_blrms = config.get('Email', 'blrms_hour')
run_dir = config.get('Paths', 'run_dir')
out_dir = config.get('Paths', 'out_dir')
pub_url = config.get('Paths', 'public_url')
thresholds_config_file = config.get('Paths', 'thresholds')

# Job directory
hist_dir = os.path.join(run_dir, 'history')
job_dir = os.path.join(run_dir, 'jobs', year_month_str, str(current_time))
# Output directories
temp_dir = os.path.join(job_dir, 'results')
results_dir = os.path.join(out_dir, 'results')
results_archive_dir = os.path.join(results_dir, 'old', year_month_str)
# Setup HTML directories
page_dir = os.path.join(out_dir, 'pages', year_month_str)
status_dir = os.path.join(out_dir, 'status')
if not os.path.exists(page_dir):
    os.makedirs(page_dir)
if not os.path.exists(status_dir):
    os.makedirs(status_dir)
if not os.path.exists(os.path.join(page_dir, 'css')):
    shutil.copytree(os.path.join(out_dir, 'css'),
                    os.path.join(page_dir, 'css'))
if not os.path.exists(os.path.join(page_dir, 'js')):
    shutil.copytree(os.path.join(out_dir, 'js'),
                    os.path.join(page_dir, 'js'))

# RESULTS HANDLING
# Combine and sort results file
results_file = os.path.join(results_dir, 'results_current.txt')
temp_files = [os.path.join(temp_dir, x) for x in os.listdir(temp_dir)]
results_lines = []
for f in temp_files:
    with open(f, 'r') as f_in:
        results_lines += f_in.readlines()
with open(results_file, 'w') as f:
    f.writelines(sorted(results_lines))
# Parse thresholds config
thresholds_config = ConfigParser()
thresholds_config.read(thresholds_config_file)
blrms_thresholds = {
    key: float(value) for key, value in thresholds_config.items('BLRMS')}
# Filter magnetometers
ok_channels = lcutils.filter_magnetometers(results_file)
# Sort data by alert status and BLRMS condition
lcutils.filter_results(results_file)
# Copy new results to archive
results_archive = os.path.join(
    results_archive_dir, 'results_%s.txt' % current_time)
shutil.copy2(results_file, results_archive)
# Update alert status in history file
hist_file = os.path.join(hist_dir, 'history.txt')
with open(results_file, 'r') as f:
    for x in f.readlines():
        s = x.rstrip().split(',')
        channel, status, hour = (s[0], s[-2], int(float(s[-1])))
        blrms_status, blrms_hour = (s[-4], int(float(s[-3])))
        lcalert.update_alert_status(
            hist_file, channel, status, hour, blrms_status, blrms_hour)

# HTML PAGES
html_page = os.path.join(
    page_dir, 'LigoCamHTML_%s.html' % current_time)
asd_path = os.path.join(
    pub_url, 'images', 'ASD', year_month_str, str(current_time))
ts_path = os.path.join(
    pub_url, 'images', 'TS', year_month_str, str(current_time))
# Create HTML page
lchtml.create_html(
    html_page, results_file, ifo, subsystem, current_time_utc,
    asd_path, ts_path, blrms_thresholds)
# Create single HTMLs
lchtml.create_single_htmls(
    status_dir, results_file, ifo, subsystem, current_time_utc,
    asd_path, ts_path, blrms_thresholds)
# Copy to current HTML for "latest page" view
filestat = os.stat(html_page)
file_size = filestat.st_size
if file_size > 2000:
    html_current = os.path.join(out_dir, 'LigoCamHTML_current.html')
    shutil.copy2(html_page, html_current)
# Create empty HTML page for missing channels
with open(channel_list, 'r') as f:
    all_channels = [line.rstrip() for line in f.readlines()]
with open(results_file, 'r') as f:
    results_channels = [line.split(',')[0] for line in f.readlines()]
for chan in all_channels:
    if chan not in results_channels:
        status_file = os.path.join(
            status_dir, '%s_status.html' % chan.replace(':', '_'))
        lchtml.create_empty_html(status_file, results_file, chan)

# CALENDAR
# Edit this month's calendar
calendar_file = os.path.join(
    out_dir, 'calendar', 'LigoCAM_%s.html' % year_month_str)
results_url = os.path.join(
    pub_url, 'pages', year_month_str, 'LigoCamHTML_%s.html' % current_time)
lcutils.edit_calendar(calendar_file, results_url, current_time)

# EMAIL ALERT
# insp_rng = lcutils.get_inspiral_range(ifo, current_time, duration)
# print("Inspiral range:", insp_rng)
# if insp_rng > 30:
#     # Send email alert if there are any bad channels (including BLRMS changes)
#     alert_dict = {
#         'BLRMS change': lcalert.find_bad_channels(
#             hist_file, 'excess', int(email_blrms)),
#         'Disconnected': lcalert.find_bad_channels(
#             hist_file, 'disconn', int(email_disconn)),
#         'DAQ failure': lcalert.find_bad_channels(
#             hist_file, 'daqfail', int(email_daqfail)),
#         'Hardware failure': lcalert.find_bad_channels(
#             hist_file, 'hardware', int(email_hardware))
#     }
#     if any(len(x) > 0 for x in alert_dict.values()):
#         results_url = os.path.join(
#             pub_url, 'pages', year_month_str,
#             'LigoCamHTML_' + str(current_time) + '.html'
#         )
#         email_text, email_subject = lcalert.write_email(
#             ifo, subsystem, results_url, alert_dict, current_time_utc)
#         lcalert.send_email(
#             email_text, email_subject, email_from, email_to_1, email_replyto)
# else:
#     email_to_2 += email_to_1
# # Send email alert if there are any bad channels (not including BLRMS changes)
# alert_dict = {
#     'Disconnected': lcalert.find_bad_channels(
#         hist_file, 'disconn', int(email_disconn)),
#     'DAQ failure': lcalert.find_bad_channels(
#         hist_file, 'daqfail', int(email_daqfail)),
#     'Hardware failure': lcalert.find_bad_channels(
#         hist_file, 'hardware', int(email_hardware))
# }
# if any(len(x) > 0 for x in alert_dict.values()):
#     results_url = os.path.join(
#         pub_url, 'pages', year_month_str,
#         'LigoCamHTML_' + str(current_time) + '.html'
#     )
#     email_text, email_subject = lcalert.write_email(
#         ifo, subsystem, results_url, alert_dict, current_time_utc)
#     lcalert.send_email(
#         email_text, email_subject, email_from, email_to_2, email_replyto)

# CLEANUP TEMPORARY FILES
shutil.rmtree(temp_dir)
